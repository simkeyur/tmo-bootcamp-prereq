var app = angular.module('CurrencyConverterApp', []);
app.controller('CurrencyConverterCtrl', function($scope, $http) {
    $scope.AppName = "TMO Bootcamp Prereq App";

    $scope.Cryptos = ["DOGE", "BTC", "ETH"];
    $scope.selectedCrypto = $scope.Cryptos[0];

    $scope.Countries = ["INR", "AED", "CAD"];
    $scope.selectedCountry = $scope.Countries[0];

    $scope.calcFunctions = ["+", "-", "/", "*"];
    $scope.selectedCalcFunctions = $scope.calcFunctions[0];

    
    $scope.convertCrypto = function() {
        $http.get("https://rest.coinapi.io/v1/exchangerate/" + $scope.selectedCrypto +"?apikey=B0C97992-A8D2-443D-8D97-EA14B59E74EF")
            .then(function(response) {
                let cryptoExchangeStack = response.data.rates;
                for(var i = 0; i < cryptoExchangeStack.length; i++)
                    {
                        if(cryptoExchangeStack[i].asset_id_quote == 'BUSD')
                        {
                            $scope.CryptoValueInUSD = cryptoExchangeStack[i].rate;
                            
                        }
                    }
                
            });
    };

    $scope.convertCurrency = function() {
        $http.get("https://free.currconv.com/api/v7/convert?q=USD_" + $scope.selectedCountry + "&compact=ultra&apiKey=a165701c3b29944c08cc")
            .then(function(response) {
                let firstKey = Object.keys(response.data)[0];
                let firstValue = response.data[firstKey];
                $scope.ConvertedValue = firstValue;
            });
    };


    $scope.Calculate = function() {
        $scope.CalcResult = eval($scope.ValueA + $scope.selectedCalcFunctions + $scope.ValueB);
    };

    function getRandomVal(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
});
